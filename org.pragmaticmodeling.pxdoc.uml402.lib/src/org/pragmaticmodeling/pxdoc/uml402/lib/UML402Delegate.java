package org.pragmaticmodeling.pxdoc.uml402.lib;

import org.eclipse.uml2.common.util.CacheAdapter;
import org.pragmaticmodeling.pxdoc.uml.common.lib.AbstractUmlDelegate;

public class UML402Delegate extends AbstractUmlDelegate {

	@Override
	protected void addVersionSpecificExcludedFeatures() {
		
	}

	@Override
	public CacheAdapter getCacheAdapter() {
		return CacheAdapter.getInstance();
	}

}
